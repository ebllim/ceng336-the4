/**********************************************************************
 *      This file contains our structs definitions. We store player's
 * coordinate in player_t struct. And monster informations monster_t
 * struct. We stored all monsters in monster_t array. You can find
 * decleration of it in the main.c file. Also player struct is declared
 * in main.c.
 * 
 **********************************************************************/

#ifndef _GAME_H
#define _GAME_H



typedef struct player_t{
    int x;
    int y;
} player_t;

typedef struct monster_t{
    int t;
    int x;
    int y;
    int rt;
    int scr;
    char active;
} monster_t;






#endif

/* End of File : game.h */
