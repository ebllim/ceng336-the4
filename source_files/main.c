/*
 *  Standard main file
 * In init we arrange asnchronous transmitter and receiver settings.
 * we set some initial values.
 */

/**********************************************************************/
/*                                                                    */
/* File name: main.c                                                  */
/*                                                                    */
/* Since:     2004-Sept-20                                            */
/*                                                                    */
/* Version:   PICos18 v2.10                                           */
/*            Copyright (C) 2003, 2004, 2005 Pragmatec.               */
/*                                                                    */
/* Author:    Designed by Pragmatec S.A.R.L.        www.pragmatec.net */
/*            MONTAGNE Xavier [XM]      xavier.montagne@pragmatec.net */
/*                                                                    */
/* Purpose:   Custom initialization and kernel start.                 */
/*                                                                    */
/* Distribution: This file is part of PICos18.                        */
/*            PICos18 is free software; you can redistribute it       */
/*            and/or modify it under the terms of the GNU General     */
/*            Public License as published by the Free Software        */
/*            Foundation; either version 2, or (at your option)       */
/*            any later version.                                      */
/*                                                                    */
/*            PICos18 is distributed in the hope that it will be      */
/*            useful, but WITHOUT ANY WARRANTY; without even the      */
/*            implied warranty of MERCHANTABILITY or FITNESS FOR A    */
/*            PARTICULAR PURPOSE.  See the GNU General Public         */
/*            License for more details.                               */
/*                                                                    */
/*            You should have received a copy of the GNU General      */
/*            Public License along with gpsim; see the file           */
/*            COPYING.txt. If not, write to the Free Software         */
/*            Foundation, 59 Temple Place - Suite 330,                */
/*            Boston, MA 02111-1307, USA.                             */
/*                                                                    */
/*          > A special exception to the GPL can be applied should    */
/*            you wish to distribute a combined work that includes    */
/*            PICos18, without being obliged to provide the source    */
/*            code for any proprietary components.                    */
/*                                                                    */
/* History:                                                           */
/* 2004/09/20 [XM]  Create this file.                                 */
/*                                                                    */
/**********************************************************************/

#pragma config OSC = HSPLL, FCMEN = OFF, IESO = OFF, PWRT = OFF, BOREN = OFF, WDT = OFF, MCLRE = ON, LPT1OSC = OFF, LVP = OFF, XINST = OFF, DEBUG = OFF

//#define __18F8722
#include "define.h"
#include "game.h"

/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/
#define DEFAULT_MODE       0


/**********************************************************************
 * Function prototypes.
 **********************************************************************/
void main (void);
void Init(void);
void StartupHook(void);
void ShutdownHook(StatusType error);
void ErrorHook(StatusType error);
void PreTaskHook(void);
void PostTaskHook(void);

extern union Timers Tmr0;

player_t player;                        // player object
monster_t monsters[25];                 // Assumed that there can not be more than 25 monsters
monster_t targetM;                      // targetM object to store target info
int durations[4] = {15, 10, 15, 30};    // durations in terms of 50ms


AppModeType SelectedMode;

/**********************************************************************
 * -------------------------- main function ---------------------------
 *
 * Setup the different alarms and start the kernel.
 *
 **********************************************************************/
void main(void)
{
  STKPTR = 0;
  SelectedMode = DEFAULT_MODE;
  Init();
  
  while(1)
  {
    StartOS(SelectedMode);
  }
}

/**********************************************************************
 * Clear all RAM memory and set PORTB to output mode.
 *
 * @return void
 **********************************************************************/
void Init(void)
{
    int i;

    /* initalization for our global variables */
    for(i = 0; i < 25 ;i++){
        monsters[i].active = 0;
    }
    player.x = 0;
    player.y = 1;
    /**/


    FSR0H = 0;
    FSR0L = 0;

    /* User setting : actual PIC frequency */
    Tmr0.lt = _40MHZ;

    /* Timer OFF - Enabled by Kernel */
    T0CON = 0x08;
    TMR0H = Tmr0.bt[1];
    TMR0L = Tmr0.bt[0];

    /* configure I/O ports */
    TRISD = 0;				// set all ports of PortD as output
    TRISE = 0;				// set all ports of PortE as output
    PORTD = 0;				// clear PortD
    PORTE = 0;				// clear PortE

    /* configure USART transmitter/receiver */
    SPBRG1 = 21;			// for 40 MHz, to have 115200 baud rate, it should be 21

    TXSTA1 = 0x04;                  // (= 00000100) 8-bit transmit, transmitter NOT enabled, asynchronous,
                                            // high speed mode
    RCSTA1 = 0x90;                   // (= 10010000) 8-bit receiver, receiver enabled, continuous receive,
                                            // serial port enabled

    /* configure the interrupts */
    INTCON = 0;			// clear interrupt register completely
    PIE1bits.TX1IE = 1;		// enable USART transmit interrupt
    PIE1bits.RC1IE = 1;		// enable USART receive interrupt
    PIR1 = 0;			// clear all peripheral flags

    INTCONbits.PEIE = 1;            // enable peripheral interrupts
    INTCONbits.GIE = 1;		// globally enable interrupts
}

/**********************************************************************
 * Hook routine called just before entering in kernel.
 *
 * @param error      IN The new error stored in buffer
 * @return error     Error level
 **********************************************************************/
void StartupHook(void)
{
}

/**********************************************************************
 * Hook routine called just after leaving the kernel.
 *
 * @param error      IN The last error detected by OS
 * @return void
 **********************************************************************/
void ShutdownHook(StatusType error)
{
}

/**********************************************************************
 * Store a new error in a global buffer keeping a track of the 
 * application history.
 *
 * @param error      IN The new error stored in buffer
 * @return void
 **********************************************************************/
void ErrorHook(StatusType error)
{
}

/**********************************************************************
 * Hook routine called just before entering in a task.
 *
 * @return void
 **********************************************************************/
void PreTaskHook(void)
{
}

/**********************************************************************
 * Hook routine called just after leaving a task.
 *
 * @return void
 **********************************************************************/
void PostTaskHook(void)
{
}

/* End of File : main.c */
