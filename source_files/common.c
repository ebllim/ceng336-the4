/**********************************************************************/
/*                                                                    */
/* File name: common.c                                                */
/*                                                                    */
/* Purpose:   Holds all the global variables and functions required   */
/*            by the tasks                                            */
/*                                                                    */
/**********************************************************************/

#include "define.h"
#include "common.h"
#include "string.h"



/**********************************************************************
 * ----------------------- GLOBAL VARIABLES ---------------------------
 **********************************************************************/
char systemState = _OPERATING;		// current state of the system; _WAITING or _OPERATING	
char transmitBuffer[5];			// holds the bytes to be transmitted
char cType;                             // holds the next command
char transmitCount;			// index to the transmitBuffer array; the current byte to be transmitted
char receiveBuffer[8];			// holds the received bytes
char rPos = 0;                          // hold the index for received buffer
char END_FLAG = 1;                      // end flag


/**/
int direction = DOWN;
int dest_x = 0;
int dest_y = 7;
int targetDir = DOWN;
int GOT_IT = 0;
int c_dest_x = 0;
int c_dest_y = 1; // to make sure that there is no monster
int first_move_F = 0;
int targetMindex = 0;


extern int game_time;
extern int r_T0;
extern int r_T1;
extern int r_T2;
extern int r_T3;
/**/
extern monster_t monsters[25];
extern player_t player;
extern int durations[];

int char2Dec(char c);
void updateMonsterCounts(int t);
void transmitData();
void dataReceived();

/**********************************************************************
 * ----------------------- GLOBAL FUNCTIONS ---------------------------
 **********************************************************************/
/*
 * recevied char is in form of hex (char), so we have to make it integer
 */
int char2Dec(char c) {
    switch(c) {
        case 'a':
            return 10;
        case 'b':
            return 11;
        case 'c':
            return 12;
        case 'd':
            return 13;
        case 'e':
            return 14;
        case 'f':
            return 15;
        case 'A':
            return 10;
        case 'B':
            return 11;
        case 'C':
            return 12;
        case 'D':
            return 13;
        case 'E':
            return 14;
        case 'F':
            return 15;
        case '0':
            return 0;
        case '1':
            return 1;
        case '2':
            return 2;
        case '3':
            return 3;
        case '4':
            return 4;
        case '5':
            return 5;
        case '6':
            return 6;
        case '7':
            return 7;
        case '8':
            return 8;
        case '9':
            return 9;
        default:
            // received char problematic
            return c-'0';
    }
}
/*
 * Updates LCD by looking at upcoming monsters
 */
void updateMonsterCounts(int t){
    switch(t) {
        case 0:
            r_T0 += 1;
            LcdPrintT0();
            break;
        case 1:
            r_T1 += 1;
            LcdPrintT1();

            break;
        case 2:
            r_T2 += 1;
            LcdPrintT2();
            break;
        case 3:
            r_T3 += 1;
            LcdPrintT3();
            break;
        default:
            // type error
            break;
    }
}

/* transmits data using serial communication */
void transmitData()
{
    if(transmitCount < 3 && !END_FLAG ){
        TXREG1 = transmitBuffer[transmitCount];
	transmitCount++;
    }
    else {  // all the bytes have been sent

        while (TXSTA1bits.TRMT == 0); // this is for last byte
	TXSTA1bits.TXEN = 0;  // disable transmitter
    }

}

/* Invoked when receive interrupt occurs; meaning that data is received */
void dataReceived()
{
    
    unsigned char receivedChar = RCREG1;
    int rI = 0;
    if(receivedChar == '$'){
        rPos = 0;
    }
    receiveBuffer[rPos++] = receivedChar;
    if (receivedChar == ':') {                                                  // stop character
//         set related events here by looking at receivedBuffer
        receiveBuffer[rPos] = 0;
//        if(0 == strcmppgm2ram(receiveBuffer, "$GO:")){
        if('G' == receiveBuffer[1]){
            SetEvent(TASK1_ID, GO_COMMAND);
//        }else if(0 == strcmppgm2ram(receiveBuffer, "$END:")){
        }else if('E' == receiveBuffer[1]){
            TXSTA1bits.TXEN = 0;                                                // disable transmitter
            END_FLAG = 1;
        }else if('D' == receiveBuffer[1]) { // $DTI:
            rI = 16 * char2Dec(receiveBuffer[4]);
            rI += char2Dec(receiveBuffer[5]);
            
            /*
             * if we get errornous moster data we simply ignore it.
             * therefore sometimes moster data doesn't match with simulator.
             */
            if(rI < 25){
                monsters[rI].active = 1;
                monsters[rI].t = char2Dec(receiveBuffer[3]);
                monsters[rI].x = (rI % 5) * 2;
                monsters[rI].y = (rI / 5) * 2;
                monsters[rI].rt = durations[monsters[rI].t];
                monsters[rI].scr = 1;

                updateMonsterCounts(monsters[rI].t);

                SetEvent(TASK0_ID, SCORE);
            }else{
                // this index received errornous
            }
           

        }else if ('P' == receiveBuffer[1]){ // $PXY
            player.x  = char2Dec(receiveBuffer[3]);
            player.y  = char2Dec(receiveBuffer[5]);
        }

        rPos = 0;
    }else {
        // not necessery !!
    }
}


/* End of File : common.c */
