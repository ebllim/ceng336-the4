/**********************************************************************
 *  In this file you can find a task that calculates the score for every
 * monster that we have currently.
 *
 *
 **********************************************************************/

#include "define.h"
#include "common.h"

#define ALARM_TSK0      0
/**********************************************************************
 * ----------------------- GLOBAL VARIABLES ---------------------------
 **********************************************************************/
#define DOABLE_C        50
#define T3_C            1000
#define T2_C            0
#define T1_C            500
#define T0_C            250


extern int first_move_F;
extern monster_t targetM;
extern monster_t monsters[];
extern int targetMindex;
extern player_t player;
extern int direction;

/**********************************************************************
 **********************************************************************/
/*
 absolute value function that takes integer value returns absolute value of it.
 */
int IabsI(int a){
    if(a < 0){
        return a*(-1);
    }
    return a;
}
/*
 * Calculates score for each monster.
 *
 */
int calculateScore(int i){
    int scr = 0;
    monster_t m = monsters[i];
    int dist_x = IabsI(player.x - m.x);
    int dist_y = IabsI(player.y - m.y);
    // can be improved
    float doable = m.rt - (dist_x + dist_y) ;

    if(doable > 0){
//        scr += (1/doable) * DOABLE_C;
        if(m.t == 3){
            scr += T3_C;
        }else if(m.t == 2){
            scr += T2_C;
        }else if(m.t == 1){
            scr += T1_C;
        }else if(m.t == 0){
            scr += T0_C;
        }

    }
    return scr;
}

/**********************************************************************
 * ------------------------------ TASK0 -------------------------------
 *
 * Task zero waits for "SCORE" event when it the event set it calculates
 * scores for every monster in order to choose the target to hunt
 *
 **********************************************************************/

TASK(TASK0) 
{
    int i;
    int scoreH = 0;
    int iH = 0;
    while(1) {
        WaitEvent(SCORE);
        ClearEvent(SCORE);
        scoreH = 0;
        for(i = 0; i < 25; i++){
            if(monsters[i].active){
                monsters[i].scr = calculateScore(i);
                if(monsters[iH].scr < monsters[i].scr){
                    iH = i;
                }
            }
        }

        // if cannot find any valuable monster to hunt move to the middle
        if(monsters[iH].active == 0){
            targetM.x = 4; //monsters[12].x;
            targetM.y = 4; //monsters[12].y;
            monsters[12].scr = 0;
            targetMindex = 12;
            first_move_F = 0;
        }else{
            targetM.x = monsters[iH].x;
            targetM.y = monsters[iH].y;
            targetMindex = iH;
            first_move_F = 0;
        }
        
    }
    TerminateTask();
}


/* End of File : tsk_task0.c */
