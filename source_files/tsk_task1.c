/**********************************************************************
 *      This file contains TASK1 that is our main task. This task waits for
 * go command then it starts to calculate what robot should do for next turn.
 *
 * You can find more explanations at function prototypes part of this file.
 **********************************************************************/

#include "define.h"
#include "common.h"

/**********************************************************************
 * Definition dedicated to the local functions.
 **********************************************************************/
#define ALARM_TSK1      0

/**********************************************************************
 * ----------------------- GLOBAL VARIABLES ---------------------------
 **********************************************************************/
extern char systemState;			// current state of the system; _WAITING or _OPERATING
extern char transmitBuffer[5];			// holds the bytes to be transmitted/displayed. format: XXYYY
extern char cType;
extern char transmitCount;			// index to the transmitBuffer array; the current byte to be transmitted
extern char receiveBuffer[8];			// holds the received bytes: format <x:y>
extern char rPos = 0;
extern char END_FLAG;
extern player_t player;
extern monster_t monsters[];
extern monster_t targetM;


extern int direction;// = DOWN;
extern int dest_x;
extern int dest_y;
extern int targetDir;// = DOWN;
extern int GOT_IT;
extern int c_dest_x;
extern int c_dest_y;
extern int first_move_F;
extern int targetMindex;

extern int game_time ;
extern int r_T0;
extern int r_T1;
extern int r_T2;
extern int r_T3;
int timeC = 0;



/**********************************************************************
 * ----------------------- FUNCTION PROTOTYPES ------------------------
 **********************************************************************/
/*
 * Helper function to see target destination info on LATD
 */
void printDests();
/*
 * Helper function to see target monster's destination info on LATD
 */
void printCDests();
/*
 * getMonster function makes robot direction to the target and set cType to 'A'
 * in order to get that monster
 */
void getMonster();
/*
 * initialize function for transmit buffer
 */
void initTransmitBuffer();
/*
 * initialize function for game
 */
void initGame();
/*
 * nextMove function calculates robot's next command by looking at it's position,
 * direction, target's position. We also look at their distance in order to select
 * optimum path. We try to minimize rotation so that we lost less amount of time to
 * get that monster. Also while we aim to some monster TASK0 can change our target,
 * therefore after change of target we try to reach new one. I mean we do not lock
 * for any aim. We try to hunt animal that have the most score.
 * 
 */
void nextMove();
/*
 * Calculates destination for robot. Since robot cannot go grass cell and each
 * grass cell has more than one edge road that robot can move, we cannot set
 * destination to the grass cell. In this function we try to get most efficent
 * edge for robot. We look for robots direction and first move to set our optimum
 * destination selection.
 */
void calculateDestination();


/**********************************************************************
 * ----------------------- LOCAL FUNCTIONS ----------------------------
 **********************************************************************/
/* Converts an unsigned number, n, to ASCCI string format */

void printDests(){
    LATD = dest_y << 4;
    LATD += dest_x;
}
void printCDests(){
    LATD = c_dest_y << 4;
    LATD += c_dest_x;
}
void initTransmitBuffer()
{
    transmitBuffer[0] = '$';
    transmitBuffer[1] = 'A';
    transmitBuffer[2] = ':';
}
void initGame(){
    int i;
    for(i = 0; i < 25 ;i++){
        monsters[i].active = 0;
        monsters[i].scr = 0;
    }
    player.x = 0;
    player.y = 1;
    direction = DOWN;
    GOT_IT = 0;
    first_move_F = 0;

    game_time = 20;
    r_T0 = 0;
    r_T1 = 0;
    r_T2 = 0;
    r_T3 = 0;
    timeC = 0;

//    ClearLCDScreen();
    LcdPrintString("X   CENG 336   X", 0, 0);
    LcdPrintString("X  CENG HUNTER X", 0, 1);
}

void nextMove(){
    int dist_x = (player.x - dest_x)*(player.x - dest_x);
    int dist_y = (player.y - dest_y)*(player.y - dest_y);
    
    // direction doable do it
    if(direction == UP && UP_DOABLE && UP_WANTED){
        if(dist_y == 1 && dist_x && !(dest_y % 2)){
            if(RIGHT_WANTED){
                cType = 'R';
                direction = RIGHT;
            }else{
                cType = 'L';
                direction = LEFT;
            }
        }else{
            cType = 'M';
            MOVE_UP;
        }
    }else if(direction == RIGHT && RIGHT_DOABLE && RIGHT_WANTED){
        if(dist_x == 1 && dist_y && !(dest_x % 2)){
            if(UP_WANTED){
                cType = 'L';
                direction = UP;
            }else{
                cType = 'R';
                direction = DOWN;
            }
        }else{
            cType = 'M';
            MOVE_RIGHT;
        }
    }else if(direction == DOWN && DOWN_DOABLE && DOWN_WANTED){
        if(dist_y == 1 && dist_x && !(dest_y % 2)){
            if(RIGHT_WANTED){
                cType = 'L';
                direction = RIGHT;
            }else{
                cType = 'R';
                direction = LEFT;
            }
        }else{
            cType = 'M';
            MOVE_DOWN;
        }
    }else if(direction == LEFT && LEFT_DOABLE && LEFT_WANTED){
        if(dist_x == 1 && dist_y && !(dest_x % 2)){
            if(UP_WANTED){
                cType = 'R';
                direction = UP;
            }else{
                cType = 'L';
                direction = DOWN;
            }
        }else{
            cType = 'M';
            MOVE_LEFT;
        }
    }
    // direction doesn't match
    else if (UP_DOABLE && UP_WANTED){ // set direction for next move to up
        if(direction == LEFT){
            cType = 'R';
            direction = UP;
        }else if(direction == RIGHT){
            cType = 'L';
            direction = UP;
        }else{//down
            if(RIGHT_WANTED && RIGHT_DOABLE){
                cType = 'L';
                direction = RIGHT;
            }else{
                cType = 'R';
                direction = LEFT;
            }
        }
    }else if (RIGHT_DOABLE && RIGHT_WANTED){ // set direction for next move to right
        if(direction == UP){
            cType = 'R';
            direction = RIGHT;
        }else if(direction == DOWN){
            cType = 'L';
            direction = RIGHT;
        }else{//left
            if(DOWN_WANTED && DOWN_DOABLE){
                cType = 'L';
                direction = DOWN;
            }else{
                cType = 'R';
                direction = UP;
            }
        }
    }else if (DOWN_DOABLE && DOWN_WANTED){ // set direction for next move to down
        if(direction == RIGHT){
            cType = 'R';
            direction = DOWN;
        }else if(direction == LEFT){
            cType = 'L';
            direction = DOWN;
        }else{//up
            if(LEFT_WANTED && LEFT_DOABLE){
                cType = 'L';
                direction = LEFT;
            }else{
                cType = 'R';
                direction = RIGHT;
            }
        }
    }else if (LEFT_DOABLE && LEFT_WANTED){ // set direction for next move to down
        if(direction == DOWN){
            cType = 'R';
            direction = LEFT;
        }else if(direction == UP){
            cType = 'L';
            direction = LEFT;
        }else{//right
            if(UP_WANTED && UP_DOABLE){
                cType = 'L';
                direction = UP;
            }else{
                cType = 'R';
                direction = DOWN;
            }
        }
    }
    // player stucks
    else if(UP_WANTED && ! UP_DOABLE){
        if(direction == LEFT){
            if(LEFT_DOABLE){
                cType = 'M';
                MOVE_LEFT;
            }else{
                cType = 'R';
                direction = UP;
            }

        }else if(direction == RIGHT){
            if(RIGHT_DOABLE){
                cType = 'M';
                MOVE_RIGHT;
            }else{
                cType = 'R';
                direction = DOWN;
            }

        }else if(direction == UP){
            if(RIGHT_DOABLE){
                cType = 'R';
                direction = RIGHT;
            }else{
                cType = 'L';
                direction = LEFT;
            }
        }else if(direction == DOWN){
            if(RIGHT_DOABLE){
                cType = 'L';
                direction = RIGHT;
            }else{
                cType = 'R';
                direction = LEFT;
            }
        }
    }else if(RIGHT_WANTED && ! RIGHT_DOABLE){
        if(direction == UP){
            if(UP_DOABLE){
                cType = 'M';
                MOVE_UP;
            }else{
                cType = 'R';
                direction = RIGHT;
            }
        }else if(direction == DOWN){
            if(DOWN_DOABLE){
                cType = 'M';
                MOVE_DOWN;
            }else {
                cType = 'R';
                direction = LEFT;
            }

        }else if(direction == RIGHT){
            if(UP_DOABLE){
                cType = 'L';
                direction = UP;
            }else{
                cType = 'R';
                direction = DOWN;
            }
        }else if(direction == LEFT){
            if(UP_DOABLE){
                cType = 'R';
                direction = UP;
            }else{
                cType = 'L';
                direction = DOWN;
            }
        }
    }else if(DOWN_WANTED && ! DOWN_DOABLE){
        if(direction == LEFT){
            if(LEFT_DOABLE){
                cType = 'M';
                MOVE_LEFT;
            }else{
                cType = 'R';
                direction = UP;
            }
        }else if(direction == RIGHT){
            if(RIGHT_DOABLE){
                cType = 'M';
                MOVE_RIGHT;
            }else{
                cType = 'R';
                direction = DOWN;
            }
        }else if(direction == UP){
            if(RIGHT_DOABLE){
                cType = 'R';
                direction = RIGHT;
            }else{
                cType = 'L';
                direction = LEFT;
            }
        }else if(direction == DOWN){
            if(RIGHT_DOABLE){
                cType = 'L';
                direction = RIGHT;
            }else{
                cType = 'R';
                direction = LEFT;
            }
        }
    }else if(LEFT_WANTED && ! LEFT_DOABLE){
        if(direction == UP){
            if(UP_DOABLE){
                cType = 'M';
                MOVE_UP;
            }else{
                cType = 'R';
                direction = RIGHT;
            }
        }else if(direction == DOWN){
            if(DOWN_DOABLE){
                cType = 'M';
                MOVE_DOWN;
            }else{
                cType = 'R';
                direction = LEFT;
            }
        }else if(direction == RIGHT){
            if(UP_DOABLE){
                cType = 'L';
                direction = UP;
            }else{
                cType = 'R';
                direction = DOWN;
            }
        }else if(direction == LEFT){
            if(UP_DOABLE){
                cType = 'R';
                direction = UP;
            }else{
                cType = 'L';
                direction = DOWN;
            }
        }
    }

    else{
        if(ARRIVED) {
            getMonster();
        }else {
            // an error occcured -- this should not work any time!
            LATD = 0xff;
        }
    }
    if(cType == 'M'){
        first_move_F = 1;
        

    }


}

void getMonster(){
    
    if(targetDir == direction){
        cType = 'A';
        GOT_IT = 1;
        first_move_F = 0;
        if(monsters[targetMindex].t == 3 && monsters[targetMindex].active){
            game_time += 5;
            LcdPrintT();
        }else if(monsters[targetMindex].t == 2 && monsters[targetMindex].active){
            game_time -= 1;
            LcdPrintT();
        }
        monsters[targetMindex].active = 0;
        monsters[targetMindex].scr = 0;

    }else if(targetDir == UP && direction == LEFT){
        cType = 'R';
        direction = UP;
    }else if(targetDir == LEFT && direction == UP){
        cType = 'L';
        direction = LEFT;
    }else if(targetDir > direction){
        cType = 'R';
        direction += 1;
        // direction %= 4;
    }else {
        cType = 'L';
        direction -= 1;
        // direction %= 4;
    }

}

void calculateDestination(){
    if(!(c_dest_x == targetM.x  && c_dest_y == targetM.y)){ // monster changed
        c_dest_x = targetM.x;
        c_dest_y = targetM.y;
        first_move_F = 0;
    }
    
    if(!first_move_F){
        if(direction == LEFT || direction == RIGHT){
            if((player.y - c_dest_y) == 1 || (player.y - c_dest_y) == -1){
                dest_y = player.y;
                dest_x = c_dest_x;
            }else if(player.x > c_dest_x){
                dest_x = c_dest_x + 1;
                dest_y = c_dest_y;
            }else if (player.x < c_dest_x){
                dest_x = c_dest_x - 1;
                dest_y = c_dest_y;
            }else{
                if((player.y - c_dest_y) == 1 && (player.y - c_dest_y) == -1 ){
                    dest_x = player.x;
                    dest_y = player.y;
                }else{
                    if(c_dest_x == 8){
                        dest_x = 7;
                    }else if (c_dest_x == 0){
                        dest_x = 1;
                    }else if(direction == RIGHT){
                        dest_x = c_dest_x + 1;
                    }else if(direction == LEFT){
                        dest_x = c_dest_x - 1;
                    }
                    dest_y = c_dest_y;
                }
            }
        }else{
            if((player.x - c_dest_x) == 1 || (player.x - c_dest_x) == -1){
                dest_x = player.x;
                dest_y = c_dest_y;
            }else if(player.y > c_dest_y){
                dest_y = c_dest_y + 1;
                dest_x = c_dest_x;
            }else if (player.y < c_dest_y){
                dest_y = c_dest_y - 1;
                dest_x = c_dest_x;
            }else{
                if((player.x - c_dest_x) == 1 && (player.x - c_dest_x) == -1 ){
                    dest_x = player.x;
                    dest_y = player.y;
                }else{
                    if(c_dest_y == 8){
                        dest_y = 7;
                    }else if (c_dest_y == 0){
                        dest_y = 1;
                    }else if(direction == DOWN){
                        dest_y = c_dest_y + 1;
                    }else if(direction == UP){
                        dest_y = c_dest_y - 1;
                    }
                    dest_x = c_dest_x;
                }
            }
        }

        // set targetDir here
        if(c_dest_x - dest_x == 1){
            targetDir = RIGHT;
        }else if(c_dest_x - dest_x == -1){
            targetDir = LEFT;
        }else if(c_dest_y - dest_y == 1){
            targetDir = DOWN;
        }else if(c_dest_y - dest_y == -1){
            targetDir = UP;
        }
    }
}
/**********************************************************************
 * ------------------------------ TASK1 -------------------------------
 *
 *      Calculates for next destination.
 *      Sets next command.
 *      Starts transmission.
 *      Reduces remaining times for mosters.
 *      Arrange remaining time for every second.
 *      Request for TASK0 to recalculation for all scores.
 **********************************************************************/
TASK(TASK1)
{
    int i;

    initTransmitBuffer();
    TRISCbits.TRISC7 = 1;
    TRISCbits.TRISC6 = 0;

    PIE1bits.RCIE = 1;		// enable USART transmit interrupt
    PIE1bits.TXIE = 1;		// enable USART transmit interrupt
    PIE1bits.TX1IE = 1;		// enable USART transmit interrupt
    PIE1bits.RC1IE = 1;		// enable USART receive interrupt



    SetRelAlarm(ALARM_TSK1, 1, 51);
    while(1) {

        if(END_FLAG){
            initGame();
            WaitEvent(GO_COMMAND);
            ClearEvent(ALARM_EVENT);
            ClearEvent(GO_COMMAND);
            END_FLAG = 0;
//            ClearLCDScreen();
            LcdPrintString(" Time:20  T0:00 ", 0, 0);
            LcdPrintString("T1:00 T2:00T3:00", 0, 1);
        }
        WaitEvent(ALARM_EVENT);
        ClearEvent(ALARM_EVENT);
//        printCDests();
//        printDests();
        calculateDestination();
        nextMove();

        transmitBuffer[1] = cType;
        transmitCount = 0;
        TXSTA1bits.TXEN = 1;


        // reduce remaining times for all active monsters
        for(i = 0; i < 25 ;i++){
            if(monsters[i].active){
                if(!(--monsters[i].rt)){
                    monsters[i].active = 0;
                }
            }
        }
        timeC++;
        if(!(timeC % 20)){
            game_time--;
            LcdPrintT();                // change Time on LCD
            timeC = 0;
        }
        SetEvent(TASK0_ID, SCORE);

    }
    
    TerminateTask();
}

/* End of File : tsk_task1.c */
