/**********************************************************************/
/*                                                                    */
/* File name: define.h                                                */
/*                                                                    */
/* Since:     2004-Sept-20                                            */
/*                                                                    */
/* Version:   PICos18 v2.10                                           */
/*            Copyright (C) 2003, 2004, 2005 Pragmatec.               */
/*                                                                    */
/* Author:    Designed by Pragmatec S.A.R.L.        www.pragmatec.net */
/*            MONTAGNE Xavier [XM]      xavier.montagne@pragmatec.net */
/*            NIELSEN  Peter  [PN]                   pnielsen@tuug.fi */
/*                                                                    */
/* Purpose:   Specify all the specific definitions of the project.    */
/*                                                                    */
/* Distribution: This file is part of PICos18.                        */
/*            PICos18 is free software; you can redistribute it       */
/*            and/or modify it under the terms of the GNU General     */
/*            Public License as published by the Free Software        */
/*            Foundation; either version 2, or (at your option)       */
/*            any later version.                                      */
/*                                                                    */
/*            PICos18 is distributed in the hope that it will be      */
/*            useful, but WITHOUT ANY WARRANTY; without even the      */
/*            implied warranty of MERCHANTABILITY or FITNESS FOR A    */
/*            PARTICULAR PURPOSE.  See the GNU General Public         */
/*            License for more details.                               */
/*                                                                    */
/*            You should have received a copy of the GNU General      */
/*            Public License along with gpsim; see the file           */
/*            COPYING.txt. If not, write to the Free Software         */
/*            Foundation, 59 Temple Place - Suite 330,                */
/*            Boston, MA 02111-1307, USA.                             */
/*                                                                    */
/*          > A special exception to the GPL can be applied should    */
/*            you wish to distribute a combined work that includes    */
/*            PICos18, without being obliged to provide the source    */
/*            code for any proprietary components.                    */
/*                                                                    */
/* History:                                                           */
/* 2004/11/06 [RZR] Original idea of RZR.                             */
/* 2007/01/01 [PN]  Added magic formula dedicated to clock frequency. */
/*                                                                    */
/**********************************************************************/

#ifndef _DEFINE_H
#define _DEFINE_H

#include "device.h"
#include "game.h"
/***********************************************************************
 * ------------------------ Timer settings -----------------------------
 **********************************************************************/
/* MAGIC : Tmr0.lt = 65536 - (CPU_FREQUENCY_HZ/4/1000 - 232)          */
#define _10MHZ	63320
#define _16MHZ	61768
#define _20MHZ	60768
#define _32MHZ	57768
#define _40MHZ 	55768

/***********************************************************************
 * ----------------------------- Events --------------------------------
 **********************************************************************/
#define ALARM_EVENT       0x80
#define GO_COMMAND        0x40
#define SCORE             0x20
#define LCD_EVENT         0x01

/***********************************************************************
 * ----------------------------- Task ID -------------------------------
 **********************************************************************/
/* Info about the tasks:
 * TASK0: 7-segment display task
 * TASK1: A/D Conversion and USART transmission
 */
#define TASK0_ID             1
#define TASK1_ID             2
#define LCD_ID               5

/* Priorities of the tasks */
#define TASK0_PRIO           8
#define TASK1_PRIO           9
#define LCD_PRIO            10


/***********************************************************************
 * --------------- DEFINEs that make code more readable ----------------
 **********************************************************************/
#define UP              0
#define RIGHT           1
#define DOWN            2
#define LEFT            3
#define UP_WANTED       (player.y > dest_y)
#define RIGHT_WANTED    (player.x < dest_x)
#define DOWN_WANTED     (player.y < dest_y)
#define LEFT_WANTED     (player.x > dest_x)

#define UP_DOABLE       ((player.x % 2) && player.y != 0)
#define RIGHT_DOABLE    ((player.y % 2) && player.x != 8)
#define DOWN_DOABLE     ((player.x % 2) && player.y != 8)
#define LEFT_DOABLE     ((player.y % 2) && player.x != 0)
#define MOVE_UP         player.y -= 1
#define MOVE_RIGHT      player.x += 1
#define MOVE_DOWN       player.y += 1
#define MOVE_LEFT       player.x -= 1
#define ARRIVED         (player.x == dest_x && player.y == dest_y)

#endif /* _DEFINE_H */


/* End of File : define.h */
