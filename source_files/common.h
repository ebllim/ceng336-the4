#ifndef _COMMON_H
#define _COMMON_H

/**********************************************************************
 * ----------------------- GLOBAL DEFINITIONS -------------------------
 **********************************************************************/

#define X 		1		// the position of Set Value, x, in the receive array
#define Y		3		// the position of Offset Value, y, in the receive array

/* System States */
#define _WAITING	0		// waiting state
#define _OPERATING	1		// operating state




/**********************************************************************
 * ----------------------- FUNCTION PROTOTYPES ------------------------
 **********************************************************************/
 /* transmits data using serial communication */
void transmitData();
/* Invoked when receive interrupt occurs; meaning that data is received */
void dataReceived();




#endif

/* End of File : common.h */
