
typedef struct player_t{
    int x;
    int y;
} player_t;

typedef struct monster_t{
    int t;
    int x;
    int y;
    int rt;
    int scr;
    char active;
} monster_t;

player_t player;

monster_t monsters[20]; // Assumed that there can not be more than 20 monsters

