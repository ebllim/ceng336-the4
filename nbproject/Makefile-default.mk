#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=cof
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=cof
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=source_files/common.c source_files/int.c source_files/main.c source_files/taskdesc.c source_files/tsk_task0.c source_files/tsk_task1.c source_files/LCD.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/source_files/common.o ${OBJECTDIR}/source_files/int.o ${OBJECTDIR}/source_files/main.o ${OBJECTDIR}/source_files/taskdesc.o ${OBJECTDIR}/source_files/tsk_task0.o ${OBJECTDIR}/source_files/tsk_task1.o ${OBJECTDIR}/source_files/LCD.o
POSSIBLE_DEPFILES=${OBJECTDIR}/source_files/common.o.d ${OBJECTDIR}/source_files/int.o.d ${OBJECTDIR}/source_files/main.o.d ${OBJECTDIR}/source_files/taskdesc.o.d ${OBJECTDIR}/source_files/tsk_task0.o.d ${OBJECTDIR}/source_files/tsk_task1.o.d ${OBJECTDIR}/source_files/LCD.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/source_files/common.o ${OBJECTDIR}/source_files/int.o ${OBJECTDIR}/source_files/main.o ${OBJECTDIR}/source_files/taskdesc.o ${OBJECTDIR}/source_files/tsk_task0.o ${OBJECTDIR}/source_files/tsk_task1.o ${OBJECTDIR}/source_files/LCD.o

# Source Files
SOURCEFILES=source_files/common.c source_files/int.c source_files/main.c source_files/taskdesc.c source_files/tsk_task0.c source_files/tsk_task1.c source_files/LCD.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F8722
MP_PROCESSOR_OPTION_LD=18f8722
MP_LINKER_DEBUG_OPTION= -u_DEBUGCODESTART=0x1fdc0 -u_DEBUGCODELEN=0x240 -u_DEBUGDATASTART=0xef4 -u_DEBUGDATALEN=0xb
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/source_files/common.o: source_files/common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/common.o.d 
	@${RM} ${OBJECTDIR}/source_files/common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/common.o   source_files/common.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/common.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/common.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/int.o: source_files/int.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/int.o.d 
	@${RM} ${OBJECTDIR}/source_files/int.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/int.o   source_files/int.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/int.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/int.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/main.o: source_files/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/main.o.d 
	@${RM} ${OBJECTDIR}/source_files/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/main.o   source_files/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/main.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/taskdesc.o: source_files/taskdesc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/taskdesc.o.d 
	@${RM} ${OBJECTDIR}/source_files/taskdesc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/taskdesc.o   source_files/taskdesc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/taskdesc.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/taskdesc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/tsk_task0.o: source_files/tsk_task0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/tsk_task0.o.d 
	@${RM} ${OBJECTDIR}/source_files/tsk_task0.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/tsk_task0.o   source_files/tsk_task0.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/tsk_task0.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/tsk_task0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/tsk_task1.o: source_files/tsk_task1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/tsk_task1.o.d 
	@${RM} ${OBJECTDIR}/source_files/tsk_task1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/tsk_task1.o   source_files/tsk_task1.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/tsk_task1.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/tsk_task1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/LCD.o: source_files/LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/LCD.o.d 
	@${RM} ${OBJECTDIR}/source_files/LCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -D__DEBUG -D__MPLAB_DEBUGGER_PICKIT2=1 -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/LCD.o   source_files/LCD.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/LCD.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/LCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
else
${OBJECTDIR}/source_files/common.o: source_files/common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/common.o.d 
	@${RM} ${OBJECTDIR}/source_files/common.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/common.o   source_files/common.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/common.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/common.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/int.o: source_files/int.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/int.o.d 
	@${RM} ${OBJECTDIR}/source_files/int.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/int.o   source_files/int.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/int.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/int.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/main.o: source_files/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/main.o.d 
	@${RM} ${OBJECTDIR}/source_files/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/main.o   source_files/main.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/main.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/taskdesc.o: source_files/taskdesc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/taskdesc.o.d 
	@${RM} ${OBJECTDIR}/source_files/taskdesc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/taskdesc.o   source_files/taskdesc.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/taskdesc.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/taskdesc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/tsk_task0.o: source_files/tsk_task0.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/tsk_task0.o.d 
	@${RM} ${OBJECTDIR}/source_files/tsk_task0.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/tsk_task0.o   source_files/tsk_task0.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/tsk_task0.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/tsk_task0.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/tsk_task1.o: source_files/tsk_task1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/tsk_task1.o.d 
	@${RM} ${OBJECTDIR}/source_files/tsk_task1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/tsk_task1.o   source_files/tsk_task1.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/tsk_task1.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/tsk_task1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
${OBJECTDIR}/source_files/LCD.o: source_files/LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/source_files" 
	@${RM} ${OBJECTDIR}/source_files/LCD.o.d 
	@${RM} ${OBJECTDIR}/source_files/LCD.o 
	${MP_CC} $(MP_EXTRA_CC_PRE) -p$(MP_PROCESSOR_OPTION) -I"PICos18_v2_10/Include" -ms -oa-  -I ${MP_CC_DIR}/../h  -fo ${OBJECTDIR}/source_files/LCD.o   source_files/LCD.c 
	@${DEP_GEN} -d ${OBJECTDIR}/source_files/LCD.o 
	@${FIXDEPS} "${OBJECTDIR}/source_files/LCD.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ -c18 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  PICos18_v2_10/Kernel/picos18.lib  PICos18_v2_10/Linker/18f8722.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "PICos18_v2_10/Linker/18f8722.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w -x -u_DEBUG -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -l"PICos18_v2_10/Kernel"  -z__MPLAB_BUILD=1  -u_CRUNTIME -z__MPLAB_DEBUG=1 -z__MPLAB_DEBUGGER_PICKIT2=1 $(MP_LINKER_DEBUG_OPTION) -l ${MP_CC_DIR}/../lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}  PICos18_v2_10/Kernel/picos18.lib 
else
dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  PICos18_v2_10/Kernel/picos18.lib PICos18_v2_10/Linker/18f8722.lkr
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_LD} $(MP_EXTRA_LD_PRE) "PICos18_v2_10/Linker/18f8722.lkr"  -p$(MP_PROCESSOR_OPTION_LD)  -w  -m"${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map" -l"PICos18_v2_10/Kernel"  -z__MPLAB_BUILD=1  -u_CRUNTIME -l ${MP_CC_DIR}/../lib  -o dist/${CND_CONF}/${IMAGE_TYPE}/the4_21.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}  PICos18_v2_10/Kernel/picos18.lib 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
